from random import randint

theirName = input("Hi! What is your name? ")

for num in range(1, 6):
    monthGuess = randint(1, 12)
    yearGuess = randint(1924, 2004)
    print("Guess", num, ":", theirName, "were you born in", monthGuess, "/", yearGuess, "?")
    theirReply = input("yes or no? ")

    if theirReply == "yes":
        print("\nI knew it!")
        exit()
    elif (theirReply == "no") & (num < 5):
        print("Drat! Lemme try again!")
    else:
        print("I have other things to do. Goodbye.")
        exit()